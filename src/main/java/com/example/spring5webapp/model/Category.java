package com.example.spring5webapp.model;

import javax.persistence.Entity;

/**
 * Created by Jorge Aguirre on 12/6/2017.
 */
@Entity
public class Category extends ModelBase {
    private String name;
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


}
