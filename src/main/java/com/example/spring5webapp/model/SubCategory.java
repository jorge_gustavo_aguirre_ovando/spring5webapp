package com.example.spring5webapp.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * Created by Jorge Aguirre on 12/6/2017.
 */
@Entity
public class SubCategory extends ModelBase {
    private String name;
    private String code;

    @OneToOne
    private Category category;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
