package com.example.spring5webapp.model;

import javax.persistence.Entity;

/**
 * Created by Jorge Aguirre on 12/6/2017.
 */
@Entity
public class Position extends ModelBase {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
