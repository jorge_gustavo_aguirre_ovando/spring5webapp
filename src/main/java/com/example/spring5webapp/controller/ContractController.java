package com.example.spring5webapp.controller;

import com.example.spring5webapp.repositories.ContractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Jorge Aguirre on 12/7/2017.
 */
@Controller
public class ContractController {
    @Autowired
    private ContractRepository contractRepository;

    @RequestMapping("/contracts")
    public String getContracts(Model model) {
        model.addAttribute("contracts", contractRepository.findAll());
        return "contracts";
    }
}
