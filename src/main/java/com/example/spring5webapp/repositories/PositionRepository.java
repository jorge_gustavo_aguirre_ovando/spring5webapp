package com.example.spring5webapp.repositories;

import com.example.spring5webapp.model.Position;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Jorge Aguirre on 12/7/2017.
 */
public interface PositionRepository extends CrudRepository<Position, Long> {
}
